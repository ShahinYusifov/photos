const express = require("express");
const userController = require("../controllers/userController");
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware')

router.route("/register").post(userController.createUser);
router.route("/login").post(userController.loginUser);
router.route("/dashboard").get(authMiddleware.authToken,userController.getDashboardPage);

module.exports = router;