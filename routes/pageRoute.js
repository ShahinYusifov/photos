const express = require("express");
const pageController = require("../controllers/pageController");
const router = express.Router();
const authMiddleware = require("../middlewares/authMiddleware");

router.route("/").get(pageController.getHomePage);
router.route("/about").get(pageController.getAboutPage);
router.route("/register").get(pageController.getRegisterPage);
router.route("/login").get(pageController.getLoginPage);
router.route("/logout").get(pageController.getLogout);

module.exports = router;