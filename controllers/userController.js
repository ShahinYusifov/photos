const User = require("../models/userModel");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Photo = require("../models/photoModel");

const createUser = async (req, res) => {
  try {
    const user = await User.create(req.body);
    res.status(201).json({user: user.id});
  } catch (error) {

    let errors2 = {}

    if(error.code === 11000) {
      errors2.email = "The email is already in use"
    }

    if(error.name === "ValidationError") {
      Object.keys(error.errors).forEach((key)=>{
        errors2[key] = error.errors[key].message;
      })
    }

    console.log("ERRORS2:::", errors2);


    res.status(400).json(errors2);
  }
};

const loginUser = async (req, res) => {
  try {
    const username = req.body.username;
    const password = req.body.password;

    const user = await User.findOne({ username: username });
    let same = false;

    if (user) {
      same = await bcrypt.compare(password, user.password);
    } else {
      return res.status(401).json({
        succeded: false,
        error: "There is no such user",
      });
    }
    if (same) {
      const token = createToken(user._id);
      res.cookie("jwt", token, {
        httpOnly: true,
        maxAge: 1000 * 60 * 60 * 24,
      });
      res.redirect( "/users/dashboard")
    } else {
      res.status(401).json({
        succeded: false,
        error: "Password is not matched !",
      });
    }
  } catch (error) {
    res.status(500).json({
      succeded: false,
      error,
    });
  }
};

const createToken = (id) => {
  return jwt.sign({ id }, process.env.JWT_SECRET_KEY, {
    expiresIn: "1d",
  });
};

const getDashboardPage = async (req, res) => {
  const photos = await Photo.find({user: res.locals.user._id} )
  res.render("dashboard",{
    link:'dashboard',
    photos: photos
  });
};

module.exports = { createUser, loginUser, getDashboardPage };
