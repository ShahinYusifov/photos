const mongoose = require('mongoose');

const connect = ()=>{
  mongoose.connect(process.env.DB_URI,{
    dbName: "photographer",
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(() => {
    console.log("Connected database successfully");
  })
  .catch((err)=>{
    console.log("Database connection error: " + err);
  })
}

module.exports = connect