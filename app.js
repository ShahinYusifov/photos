const express = require("express");
const dotenv = require("dotenv");
const connect = require("./dbConnection");
const pageRoute = require("./routes/pageRoute");
const photoRoute = require("./routes/photoRoute");
const userRoute = require("./routes/userRoute");
const cookieParser = require("cookie-parser");
const checkUser = require("./middlewares/authMiddleware")
const fileUpload = require("express-fileupload");
const cloudinary = require("cloudinary").v2;

dotenv.config();

cloudinary.config({
  cloud_name: process.env.CLOUD_NAME,
  api_key:process.env.CLOUD_API_KEY,
  api_secret:process.env.CLOUD_SECRET_KEY
})

//connect to the DB
connect();

const app = express();
const port = process.env.PORT;

//ejs template engine
app.set("view engine", "ejs");

//static files middleware
app.use(express.static("public"));
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cookieParser()); 
app.use(fileUpload({useTempfiles: true}));

//routes
app.use("*", checkUser.checkUser)
app.use("/", pageRoute);
app.use("/photos/", photoRoute);
app.use("/users/", userRoute);

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
